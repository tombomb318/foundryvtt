Frequently Asked Questions
**************************

How far along is this product in development? What is the timeline for testing phases? When will this be released?
------------------------------------------------------------------------------------------------------------------

I am an independent developer working on Foundry Virtual Tabletop as a hobby project, not as a full-time job. However,
progress on software development has gone extremely quickly with a full release anticipated in Q1 or Q2 of 2020. The
release date has not been announced, and is subject to change depending on the pace of development, the quality of
implemented features, and the amount of work still remaining in order to satisfy my original product vision.

The software was in pre-alpha development from August 2018 through November 2018 with closed Alpha testing until
February 2019. From February 2019 until present the software has been in closed Beta testing with a large and dedicated
community actively working together to get the software ready for release.

The ongoing Beta testing process if organized through Patreon until full software release and is used to actively
collect feedback and continue improving the software along the path towards a full release. Beta testing is continuous
without interruption, allowing you to use the current state of the software and receive ongoing updates as long as you
maintain your Patreon support.

Please consider supporting the project and gaining access to ongoing Beta testing by supporting the project on Patreon
at https://patreon.com/foundryvtt.

-------

Why are you creating this software?
-----------------------------------

My primary investment in this project is not as a business venture but rather as a fan of the tabletop gaming genre
and a dungeon master who loves creating rich experiences for players. I believe the tabletop RPG community deserves
better from its software and I want to contribute towards that by creating technology that I would love to use and 
I think there's a lot of opportunity for my work to benefit the gaming community.

-------

What will this cost? What is your business model?
-------------------------------------------------

While my foremost objective is not business development, I do want to approach the project with a level of seriousness 
that could allow for it to grow and support whatever community chooses to form around it.

Foundry VTT will be sold as individual licenses sold for a moderate fee. I plan to price each individual license 
somewhere in the $30 to $50 range depending on the extent of features that FVTT offers and the quality of the product
at the time of release. 

Each individual license will allow a game master to host games for unlimited players (who do not need to purchase). 
I have no plans at this time to offer any sort of subscription service and there will not be any premium features or 
gated purchase tiers.

Additionally, I have a Patreon page where community members who wish to provide additional support and have more 
direct developer access to participate in development vision and prioritization discussions. Patreon pledges are 
absolutely not required and do not modify game features, but during the pre-release phases of the software Patreon 
supporters do recieve early access to the software. 

For Patreon supporters who have contributed towards the project during the testing phases, I plan to offer a discount
on the final purchase price based on the amount or length of support during the testing process.

-------

What are the hardware requirements to use Foundry VTT?
------------------------------------------------------

It is not possible to share exact hardware requirements for the software because performance of the application depends
heavily on the type of content and features that are used within a World. As a general rule of thumb for common usage
patterns, the following hardware would be recommended:

**Minimum Requirements:**

* Relatively modern computer running Windows, MacOS, or Linux operating systems.

* An integrated GPU to enable hardware acceleration.

* 8GB of RAM

* A monitor no smaller than 1366x786. At this minimum resolution many aspects of the UI will feel cramped.

* Download/upload network speed of 5MB/s or faster.

**Recommended Hardware:**

* A discrete GPU for better rendering performance of large maps.

* A monitor with 1920x1080 or higher resolution.

* A mouse. You *can* use the software with a touchpad but the current software is designed for mouse and keyboard.

.. note:: Hardware requirements are the same for both players and for the Gamemaster.

-------

How long will Beta testing last? What does Beta testing mean for Alpha supporters?
----------------------------------------------------------------------------------

The $10 Patreon tier ("Council") was initially the tier which had access to closed Alpha testing. This supporter tier
continues to have access to the software through release along-side with Beta tier testers. The key difference for
Council tier supporters is that they gain access to major updates 5-7 days before those changes are released for all
Beta testers. This jointly reflects the fact that major changes are a bit more prone to errors and closer to an Alpha
software experience while also giving a way to reward more invested testers for their support of the project.

-------

How frequently do the software updates occur?
---------------------------------------------

Since the beginning of Alpha testing in November, 2018 I have been maintaining a pace of releasing a new update version 
approximately every 10-14 days. As the software approaches launch, the pace of updates has slowed slightly in order to
be more cautious about not introducing new bugs or regressions. Since mid 2019 now software updates have been released
every 14-21 days. You can view the change notes and dates in the archive of `_patchnotes`.

-------

Will there be an open beta that is available to anyone, free of charge?
-----------------------------------------------------------------------

Not at this time. My plans are to only do a closed beta for Patreon supporters until the full software release.

-------

Will worlds or content created using the Beta build be valid for use once the release is made available?
--------------------------------------------------------------------------------------------------------

Yes. All your content or creations will carry forward. I do not anticipate any significant issues with changes that break 
backwards compatibility. Please feel empowered to begin creating worlds that you plan to use for a long time!

-------


Thanks and Attributions
=======================

Audio
-----

* Thanks to Mike Koenig, and other Contributors: https://soundbible.com

* Thanks to Dymewiz and other Contributors: https://freesound.org

Artwork and Icons
-----------------

* Thanks to J. W. Bjerk (eleazzar) for "Painterly Spell Icons" series: https://opengameart.org

* Thanks to Contributors, https://game-icons.net

* Potion Artwork** Thanks to Melle, https://opengameart.org/content/fantasy-potion-set

* Dice by Mike Valstar from the Noun Project

* Dice by Dank By Design from the Noun Project

* Dice by Heberti Almeida from the Noun Project